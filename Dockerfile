# FROM golang:1.16
# ENV GO111MODULE=auto

# RUN go get -d -v golang.org/x/net/html  
# COPY app.go ./
# RUN CGO_ENABLED=0 go build -a -installsuffix cgo -o app .

# CMD ["./app"]
FROM python:3.12.0b1-alpine3.18
ARG user=admin
ARG email=admin@example.com
ARG password=pass
WORKDIR /app

COPY requirements.txt /app/requirements.txt
RUN mkdir db && \
    pip3 install -r requirements.txt --no-cache-dir

COPY . /app

EXPOSE 8000

# ENTRYPOINT [ "python3" ]
# CMD [ "manage.py", "runserver", "0.0.0.0:8000" ]

CMD sh init.sh && python3 manage.py runserver 0.0.0.0:8000